﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using UnityEngine.Advertisements;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class GameSys : MonoBehaviour
{

    enum GameState
    {
        Menu,
        Shop,
        Play,
        Pause
    }

    GameState gameState;

    enum Continued
    {
        Play,
        Rewarded
    }

    Continued continued;

    [SerializeField] GameObject ShopBackButton;
    [SerializeField] GameObject ShopForwardButton;
    [SerializeField] GameObject Marker;
    [SerializeField] GameObject MenuUI;
    [SerializeField] GameObject PauseUI;
    [SerializeField] GameObject GameUI;
    [SerializeField] public GameObject ShopUI;
    [SerializeField] GameObject GameOverUI;

    [SerializeField] GameObject[] spawnZones;
    Vector3[] spawnPositions = new Vector3[6]; 
    
    [SerializeField] GameObject[] ShrinkPrefs;
    [SerializeField] public GameObject NotEnough;
    [SerializeField] Text GameScore;
    [SerializeField] Text GameOverScore;
    [SerializeField] Text HighScore;
    [SerializeField] Text MenuHighScore;
    [SerializeField] Text GameOverCocoins;
    [SerializeField] Text ShopCocoins;
    [SerializeField] Material Green;
    [SerializeField] Material Purple;
    [SerializeField] Material Red;
    [SerializeField] Button BuyButton;

    [SerializeField] public Item[] Items;
    internal Button[] ShopButtons;
    [SerializeField] public GameObject[] IAPButtons;
    [SerializeField] public GameObject[] ShopModels;

    public Dictionary<string, int> Id_To_Num = new Dictionary<string, int>();

    GameObject currentMarker;
    GameObject currentShrink;
    GameObject flyingShrink;

    Quaternion camQuat;
    Vector3 camVector;

    [SerializeField] public GameObject Tap;
    [SerializeField] public GameObject TapAgain;

    static public List<GameObject> shrinks = new List<GameObject>();

    int maxFloor;
    int shopPage;
    int shopPages;

    public float markerPosX;
    public float markerPosY;

    int cocoins;
    public int Cocoins
    {
        get
        {
           return cocoins;
        }
        set
        {
            if(!isOvered||gameState==GameState.Shop)
            {
                PlayerPrefs.SetInt("Cocoins", cocoins = value);
                GameOverCocoins.text = ShopCocoins.text = value.ToString();
            }
        }
    }

    bool isOvered;
    public bool IsOvered
    {
        get
        {
            return isOvered;
        }

        set
        {
            isOvered = value;
        }
    }

    bool isPlaing;
    public bool IsPlaing
    {
        get
        {
            return isPlaing;
        }

        set
        {
            isPlaing = value;
        }
    }

    float speed;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    float currentSpeed;

    public float CurrentSpeed
    {
        get
        {
            return currentSpeed;
        }

        set
        {
            currentSpeed = value;
        }
    }

    float forceX;
    public float ForceX
    {
        get
        {
            return forceX;
        }
        set
        {
            forceX = value;
        }
    }

    public static GameSys Instance;

    int score;

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            if (!isOvered&value!=0)
            {
                score = value;
                if (score % 5 == 0)
                    speed += 0.15f;
                GameScore.text = GameOverScore.text = value.ToString();
                Cocoins++;
                if (score == 5)
                {
                    StartCoroutine(AddPlatform(spawnZones[2]));
                    StartCoroutine(AddPlatform(spawnZones[3]));
                }
                if (score == 20)
                {
                    StartCoroutine(AddPlatform(spawnZones[4]));
                    StartCoroutine(AddPlatform(spawnZones[5]));
                }
            }
        }
    }

    int floor;

    public int Floor
    {
        get
        {
            return floor;
        }

        set
        {
            floor = value;
        }
    }

    int hardness;

    public int Hardness
    {
        get
        {
            return hardness;
        }

        set
        {
            hardness = value;
        }
    }

    private void Start()
    {
        string appKey = "3fa6a6a48a65b08a9e55999a12031e7bd167fbee5879d83e";
        Appodeal.initialize(appKey, Appodeal.BANNER);
        Appodeal.show(Appodeal.BANNER_TOP);
    }
    void Awake()
    {
        Input.simulateMouseWithTouches = true;
        for (int i = 0; i < 6; i++)
        {
            spawnPositions[i] = spawnZones[i].transform.position;
        }
        cocoins = PlayerPrefs.GetInt("Cocoins");
        GameOverCocoins.text = ShopCocoins.text = cocoins.ToString();
        shopPages = Items.Length / 4;
        if (Items.Length % 4 != 0)
            shopPages++;
        camVector = Camera.main.transform.position;
        camQuat = Camera.main.transform.rotation;
        gameState = GameState.Menu;
        PauseUI.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
        if (!PlayerPrefs.HasKey("Record"))
            PlayerPrefs.SetInt("Record", 0);
        if (!PlayerPrefs.HasKey(Items[0].Name))
            PlayerPrefs.SetInt(Items[0].Name, 1);
        if (!PlayerPrefs.HasKey("tutorial"))
            PlayerPrefs.SetInt("tutorial", 0);
        if (!PlayerPrefs.HasKey("Cocoins"))
            PlayerPrefs.SetInt("Cocoins", 0);
        if (!PlayerPrefs.HasKey("Chosen"))
            PlayerPrefs.SetInt("Chosen", 0);

        HighScore.text = MenuHighScore.text = PlayerPrefs.GetInt("Record").ToString();
        MenuUI.SetActive(true);
        GameOverUI.SetActive(false);
        GameUI.SetActive(false);
        ShopUI.SetActive(false);
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DrowShop();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (gameState)
            {
                case GameState.Menu:
                    Application.Quit();
                    break;
                case GameState.Shop:
                    GoToMenu();
                    break;
                case GameState.Play:
                    Pause();
                    break;
                case GameState.Pause:
                    UnPause();
                    break;
            }
        }

    }

    private void OnApplicationQuit()
    {
        if (gameState == GameState.Play || gameState == GameState.Pause)
            GameOver();
    }

    public void ShrinkSet()
    {
        Renderer renderer;
        markerPosX = 0;
        int rnd = Random.Range(0, 6);
        floor = Random.Range(0, maxFloor);
        currentShrink = Instantiate(Items[PlayerPrefs.GetInt("Chosen")].Pref, spawnZones[floor].transform, false);
        if (rnd == 0)
        {
            hardness = 2;
            renderer = currentShrink.GetComponent<Renderer>();
            renderer.material = Red;
        }
        else if (rnd == 1 || rnd == 2)
        {
            hardness = 1;
            renderer = currentShrink.GetComponent<Renderer>();
            renderer.material = Purple;
        }
        else
        {
            hardness = 0;
            renderer = currentShrink.GetComponent<Renderer>();
            renderer.material = Green;
        }
        flyingShrink = currentShrink;
        shrinks.Add(currentShrink);
        currentSpeed = Speed + Hardness * 0.25f;
        ForceX = CurrentSpeed;
        switch (Floor / 2)
        {
            case 0:
                ForceX -= 0.08f;
                break;
            case 1:
                ForceX -= 0.5f;
                break;
            case 2:
                ForceX -= 1.2f;
                break;
        }
        markerPosX = ForceX * Mathf.Sqrt((2 * (0 + 16 + spawnZones[floor].transform.position.y))
                / -Physics.gravity.y) + 0.3f;
        markerPosY = spawnZones[floor].transform.position.y;
        if (floor % 2 == 0)
            currentMarker = Instantiate(Marker, new Vector3(-markerPosX, markerPosY + 0.01f, -0.01f), new Quaternion());
        else
            currentMarker = Instantiate(Marker, new Vector3(markerPosX, markerPosY + 0.01f, -0.01f), new Quaternion());
    }

    public void GameOver()
    {
        Shrink.tapAvailable = false;
        Destroy(currentShrink);
        Destroy(flyingShrink);
        MarkerDestroy();
        MarkerDestroy();
        PauseUI.SetActive(false);
        //MenuUI.SetActive(false);
        GameUI.SetActive(false);
        isOvered = true;
        isPlaing = false;
        if (PlayerPrefs.GetInt("Record") < Score)
        {
            PlayerPrefs.SetInt("Record", Score);
            HighScore.text = MenuHighScore.text = PlayerPrefs.GetInt("Record").ToString();
        }
        PlayerPrefs.SetInt("tutorial", 2);
    }

    public void GameOverInGame()
    {
        Shrink.tapAvailable = false;
        StartCoroutine(CleanObjs(1f));
        if (PlayerPrefs.GetInt("Record") < Score)
        {
            PlayerPrefs.SetInt("Record", Score);
            HighScore.text = MenuHighScore.text = PlayerPrefs.GetInt("Record").ToString();
        }
        PlayerPrefs.SetInt("tutorial", 2);
    }


    public void MarkerDestroy()
    {
        Destroy(currentMarker);
    }

    public void Shop()
    {
        gameState = GameState.Shop;
        MenuUI.SetActive(false);
        ShopUI.SetActive(true);
        Camera.main.transform.position = new Vector3();
        Camera.main.transform.rotation = Quaternion.Euler(0, 180, 0);

    }

    public void GoToMenu()
    {
        gameState = GameState.Menu;
        Camera.main.transform.rotation = camQuat;
        Camera.main.transform.position = camVector;
        StartCoroutine(Activate());
        PauseUI.SetActive(false);
        isPlaing = false;
        ShopUI.SetActive(false);
        GameOverUI.SetActive(false);
        GameUI.SetActive(false);
        Time.timeScale = 1;
        ClearStage();
        MenuUI.SetActive(true);
    }

    public void Pause()
    {
        gameState = GameState.Pause;
        Time.timeScale = 0;
        IsPlaing = false;
        PauseUI.SetActive(true);
        GameUI.SetActive(false);
    }

    public void UnPause()
    {
        gameState = GameState.Play;
        Time.timeScale = 1;
        IsPlaing = true;
        PauseUI.SetActive(false);
        GameUI.SetActive(true);
    }
    
    public void StartGame()
    {
        score = 0;
        GameScore.text = GameOverScore.text = score.ToString();
        for (int i = 0; i < 6; i++)
        {
            spawnZones[i].transform.position = spawnPositions[i];
        }
        Shrink.tapAvailable = true;
        maxFloor = 2;
        gameState = GameState.Play;
        ClearStage();
        IsOvered = false;
        Time.timeScale = 1;
        isPlaing = true;
        GameUI.SetActive(true);
        Speed = 3f;
        ShrinkSet();
        isPlaing = true;
        GameOverUI.SetActive(false);
        MenuUI.SetActive(false);
        continued = Continued.Play;
    }

    public void ShowRewardedAd()
    {
        if(continued == Continued.Play)
            if (Advertisement.IsReady("rewardedVideo"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResult };
                Advertisement.Show("rewardedVideo", options);
            }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");
                RewardVideo();
                break;
            case ShowResult.Skipped:
                //Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                //Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    public void StartTutorial()
    {
        PlayerPrefs.SetInt("tutorial", 0);
        StartGame();
    }

    IEnumerator CleanObjs(float time)
    {
        MenuUI.SetActive(false);
        GameUI.SetActive(false);
        MarkerDestroy();
        isOvered = true;
        isPlaing = false;
        GameOverUI.SetActive(true);
        yield return new WaitForSeconds(time);
        PauseUI.SetActive(false);
        
    }

    static void ClearStage()
    {
        for (int i = 0, c = Screw.shrinks.Count; i < c; i++)
        {
            Destroy(Screw.shrinks[i]);
        }
        Screw.shrinks.Clear();
        for (int i = 0, c = shrinks.Count; i < c; i++)
        {
            Destroy(shrinks[i]);
        }
        shrinks.Clear();
    }

    public void DrowShop()
    {
        shopPage = 1;
        ShopButtons = new Button[Items.Length];
        ShopModels = new GameObject[Items.Length];
        BuyBtn.BtnCount = 0;
        int itemCount = 0;
        float xpos = -260f;
        float ypos = 0f;
        float xposM = 2f;
        float yposM = 4.3f;
        int columns = (Items.Length + 1) / 2;
        for (int i = 0; i < columns; i++)
        {
            ypos = 260;
            yposM = 2.3f;
            for (int j = 0; j < 2; j++)
            {
                if (itemCount < Items.Length)
                {
                    ShopButtons[itemCount] = Instantiate(BuyButton, new Vector3(xpos, ypos), new Quaternion());
                    ShopButtons[itemCount].transform.SetParent(ShopUI.transform, false);
                    ShopModels[itemCount] = Instantiate(Items[itemCount].Pref);
                    ShopModels[itemCount].transform.position = new Vector3(xposM, yposM, -13);
                    ShopModels[itemCount].transform.rotation = Quaternion.Euler(-30, 90, -25);
                    Destroy(ShopModels[itemCount].GetComponent<Shrink>());
                    if (itemCount > 3)
                    {
                        ShopButtons[itemCount].gameObject.SetActive(false);
                        ShopModels[itemCount].SetActive(false);
                    }
                    yposM -= 4;
                    ypos -= 520;
                    itemCount++;
                }
            }
            if ((i + 1) % 2 == 0)
            {
                xpos = -780;
                xposM = 6;
            }
            xposM -= 4;
            xpos += 520;
        }
        ShopBackButton.SetActive(false);
        if (Items.Length < 4)
        {
            ShopForwardButton.SetActive(false);
        }
    }

    public void ShopBack()
    {
        for (int i = shopPage * 4 - 4; i < shopPage * 4; i++)
        {
            if (i < Items.Length)
            {
                ShopButtons[i].gameObject.SetActive(false);
                ShopModels[i].SetActive(false);
            }
            if (i - 4 >= 0)
            {
                ShopButtons[i - 4].gameObject.SetActive(true);
                ShopModels[i - 4].SetActive(true);
            }
        }
        shopPage--;
        if (shopPage == 1)
        {
            ShopBackButton.SetActive(false);
        }
        ShopForwardButton.SetActive(true);
    }

    public void ShopForward()
    {
        for (int i = shopPage * 4 - 4; i < shopPage * 4; i++)
        {
            if (i < Items.Length)
            {
                ShopButtons[i].gameObject.SetActive(false);
                ShopModels[i].SetActive(false);
            }
            if (i + 4 < Items.Length)
            {
                ShopButtons[i + 4].gameObject.SetActive(true);
                ShopModels[i + 4].SetActive(true);
            }
        }
        shopPage++;
        if (shopPage == shopPages)
        {
            ShopForwardButton.SetActive(false);
        }
        ShopBackButton.SetActive(true);
    }

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(0.1f);
        MenuUI.SetActive(true);
    }

    IEnumerator AddPlatform(GameObject pl)
    {
        for (int i = 0; i < 100; i++)
        {
            pl.transform.Translate(new Vector3(0.045f,0),Space.Self);
            yield return new WaitForSeconds(0.01f);
            
        }
        if (score < 20)
            maxFloor = 4;
        else
            maxFloor = 6;
    }

    public void RewardVideo()
    {
        continued = Continued.Rewarded;
        GameUI.SetActive(true);
        GameOverUI.SetActive(false);
        isOvered = false;
        isPlaing = true;
        gameState = GameState.Play;
        IsPlaing = true;
        ShrinkSet();
        Shrink.tapAvailable = true;
    }

    public void AddCoins(int value)
    {
        Cocoins += value;
    }

    public void PurchaseIAP(int butNum)
    {
        Debug.Log("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
        /*int butNum;
        Id_To_Num.TryGetValue(id,out butNum);*/
        ShopButtons[butNum].GetComponent<BuyBtn>().Click(butNum);
    }
    
}