﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Shop Item", order = 51)]

public class Item : ScriptableObject {

    [SerializeField] internal string Name;
    [SerializeField] internal int Cost;
    [SerializeField] internal GameObject Pref;
    [SerializeField] internal string Id = "aaa";
}
