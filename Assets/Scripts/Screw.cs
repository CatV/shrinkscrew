﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screw : MonoBehaviour
{

    static public List<GameObject> shrinks = new List<GameObject>();
    public GameObject collider;

    private void OnTriggerEnter(Collider other)
    {
        if (Mathf.Abs(transform.position.x - other.transform.position.x) < 0.35f & Mathf.Abs(other.transform.rotation.x) < 6)
        { 
            collider = other.gameObject;
            shrinks.Add(collider);
            float y = collider.transform.position.y;
            collider.transform.position = new Vector3(transform.position.x, y);
            GameSys.Instance.Score++;
            Rigidbody rigidbody = other.GetComponent<Rigidbody>();
            rigidbody.isKinematic = true;
            for (int i = 0; i < shrinks.Count; i++)
            {
                StartCoroutine(TwistShrink(shrinks[i]));
            }
            other.transform.rotation = Quaternion.Euler(0, 0, -90);
        }
    }

    IEnumerator TwistShrink(GameObject obj)
    {
        if (obj != null)
            for (int i = 0; i < 25; i++)
            {
                obj.transform.Rotate(0, 5, 0, Space.World);
                obj.transform.Translate(new Vector3(0, -0.022f, 0), Space.World);
                yield return new WaitForSeconds(0.01f);
            }
        if (shrinks.Count > 9)
        {
            Destroy(shrinks[0]);
            shrinks.RemoveAt(0);
        }
    }

    IEnumerator Push()
    {
        yield return new WaitForSeconds(0.0f);
        for (int i = 0; i < shrinks.Count; i++)
        {
            StartCoroutine(TwistShrink(shrinks[i]));
        }
    }
}


