﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Voidzone : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);       
    }
}