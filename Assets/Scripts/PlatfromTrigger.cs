﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatfromTrigger : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        Rigidbody rigid;
        rigid = other.GetComponent<Rigidbody>();
        rigid.isKinematic = false;
    }
}
