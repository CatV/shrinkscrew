﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class BuyBtn : MonoBehaviour
{
    Product product;
    GameObject currentIAP;
    Button button;
    Text text;
    public Text textIAP;
    internal static int BtnCount = 0;
    int ButtonNum;

    private void Awake()
    {
        PlayerPrefs.DeleteAll();
        ButtonNum = BtnCount;
        BtnCount++;
        button = gameObject.GetComponent<Button>();
        text = button.GetComponentInChildren<Text>();
        if (PlayerPrefs.GetInt(GameSys.Instance.Items[ButtonNum].Name) != 1)
        {
            button.image.color = Color.gray;
            text.text = GameSys.Instance.Items[ButtonNum].Cost.ToString();
        }
        else
        {
            text.text = "";
        }
        if (PlayerPrefs.GetInt("Chosen") == ButtonNum)
        {
            button.image.color = Color.yellow;
        }
        if (GameSys.Instance.Items[ButtonNum].Id != "aaa" & !PlayerPrefs.HasKey(GameSys.Instance.Items[ButtonNum].name))
        {
            //currentIAP = GameSys.Instance.IAPButtons[ButtonNum];
            GameSys.Instance.IAPButtons[ButtonNum].transform.SetParent(transform,false);
            
            //currentIAP = Instantiate(GameSys.Instance.IAPButtons[ButtonNum], transform, false);
            product = CodelessIAPStoreListener.Instance.GetProduct(GameSys.Instance.Items[ButtonNum].Id);
            Debug.Log(ButtonNum);
            textIAP = GameSys.Instance.IAPButtons[ButtonNum].GetComponentInChildren<Text>();
            if (textIAP != null)
                textIAP.text = product.metadata.localizedPriceString;
        }
    }

    public void Click()
    {
        Debug.Log(GameSys.Instance.Items[ButtonNum].Cost);
        Debug.Log(PlayerPrefs.GetInt("Cocoins"));
        if (PlayerPrefs.GetInt(GameSys.Instance.Items[ButtonNum].Name) != 1)
        {
            if (PlayerPrefs.GetInt("Cocoins") < GameSys.Instance.Items[ButtonNum].Cost)
            {
                StartCoroutine(showNotEnough());
            }
            else
            {
                text.text = "";
                PlayerPrefs.SetInt("Cocoins", PlayerPrefs.GetInt("Cocoins") - GameSys.Instance.Items[ButtonNum].Cost);
                PlayerPrefs.SetInt(GameSys.Instance.Items[ButtonNum].Name, 1);
                GameSys.Instance.ShopButtons[PlayerPrefs.GetInt("Chosen")].image.color = Color.white;
                PlayerPrefs.SetInt("Chosen", ButtonNum);
                GameSys.Instance.ShopButtons[ButtonNum].image.color = Color.yellow;
            }
        }
        else
        {
            if (PlayerPrefs.GetInt(GameSys.Instance.Items[ButtonNum].Name) == 1)
                GameSys.Instance.ShopButtons[PlayerPrefs.GetInt("Chosen")].image.color = Color.white;
            PlayerPrefs.SetInt("Chosen", ButtonNum);
            GameSys.Instance.ShopButtons[ButtonNum].image.color = Color.yellow;
        }

    }

    public void Click(int Num)
    {
        Destroy(GameSys.Instance.IAPButtons[ButtonNum]);
        text.text = "";
        PlayerPrefs.SetInt(GameSys.Instance.Items[ButtonNum].Name, 1);
        GameSys.Instance.ShopButtons[PlayerPrefs.GetInt("Chosen")].image.color = Color.white;
        PlayerPrefs.SetInt("Chosen", ButtonNum);
        GameSys.Instance.ShopButtons[ButtonNum].image.color = Color.yellow;
    }

    IEnumerator showNotEnough()
    {
        GameSys.Instance.NotEnough.SetActive(true);
        GameSys.Instance.NotEnough.transform.SetAsLastSibling();
        yield return new WaitForSecondsRealtime(1f);
        GameSys.Instance.NotEnough.SetActive(false);
    }
}
