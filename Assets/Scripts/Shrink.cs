﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrink : MonoBehaviour
{
    bool taped;
    Rigidbody rigid;
    public static bool tapAvailable;
    Vector3 scale;

    void Awake()
    {
        scale = transform.localScale;
        tapAvailable = false;
        StartCoroutine(Activate());
        transform.localPosition = new Vector3(-0.5f, 1.5f);
        transform.SetParent(null, true);

        taped = false;
        rigid = gameObject.GetComponent<Rigidbody>();
        rigid.isKinematic = true;
        //transform.localScale = new Vector3(0.5f, 1, 1);
        transform.localScale = scale;
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("tutorial") < 2
            & Mathf.Abs(Mathf.Abs(transform.position.x) - GameSys.Instance.markerPosX) <= 0.03f & 
            Mathf.Abs(GameSys.Instance.markerPosY - transform.position.y) <1)
        {
            Time.timeScale = 0;
            if (PlayerPrefs.GetInt("tutorial") == 1)
            {
                GameSys.Instance.TapAgain.SetActive(true);
            }
            if (PlayerPrefs.GetInt("tutorial")==0)
            {
                GameSys.Instance.Tap.SetActive(true);
            }
        }
        if (rigid.isKinematic & !taped)
        {
            transform.Translate(new Vector3(1f * GameSys.Instance.CurrentSpeed * Time.deltaTime, 0, 0), Space.Self);
        } 
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) 
            & rigid.isKinematic & !taped & GameSys.Instance.IsPlaing & tapAvailable)
        {
            //tapAvailable = false;
            if(PlayerPrefs.GetInt("tutorial")<2)
                PlayerPrefs.SetInt("tutorial", PlayerPrefs.GetInt("tutorial") + 1);
            GameSys.Instance.Tap.SetActive(false);
            GameSys.Instance.TapAgain.SetActive(false);
            Time.timeScale = 1;
            rigid.isKinematic = false;
            taped = true;
            if (transform.rotation.y > 0)
            {
                rigid.AddForce(new Vector3(-GameSys.Instance.ForceX, 16, 0), ForceMode.VelocityChange);
            }
            else
            {
                rigid.AddForce(new Vector3(GameSys.Instance.ForceX, 16, 0), ForceMode.VelocityChange);
            }
            StartCoroutine(Rotate());
            GameSys.Instance.MarkerDestroy();
            GameSys.Instance.ShrinkSet();
        }
        /*if (Input.touchCount==0)
        {
            tapAvailable = true;
        }*/
    }


    IEnumerator Rotate()
    {
        for (int i = 0; i < 45; i++)
        {
            transform.Rotate(0, 0, -2f, Space.Self);
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(0.15f);
        tapAvailable = true;
    }
}
